#_____importer les bib_____
from tkinter import *
from tkinter import ttk
import tkinter
from tkinter import filedialog
from tkinter import messagebox  
import pandas as pd
import numpy as np
import cv2
import os
import mahotas
from matplotlib import pyplot as plt
import joblib



window = Tk()
window.geometry('685x480')
window.resizable(False,False)
window.title('home')




#_______background___________
bg=PhotoImage(file='/home/soumia/py project/img.png')
img=Label(window,image=bg)
img.pack()
#____window find_____
def find (): 
    w= Tk()
    w.geometry('500x500')
    w.resizable(False,False)
    w.title('find')
    w.config(background='#5aa65c')
   
    label1=ttk.Label(w, text = "Choisis une plante pour trouver une image ",  
          background = "#e5e7e9", foreground ="black",  
          font = ("Times New Roman", 15))
    label1.place(x=50,y=100) 
    n = tkinter.StringVar() 
    plants =ttk.Combobox(w)
     
    plants['values'] = ('Tomato','Strawberry','Squash','Soybean','Raspberry','Potato','Pepper','Peach','Orange','Grape','Corn','Cherry','Blueberry','Apple') 
     
    plants.current(0)
    plants.place(x=50,y=200)
#_____2eme liste & affichage des photo___________________ 
    def elementselected (self):
        s=plants.get() 
        if s=='Tomato':
            c=ttk.Combobox(w)
            c['values']=('Yellow_Leaf_Curl_Virus','mosaic_virus','Target_Spot','Spider_mites','Septoria_leaf_spot','Leaf_Mold','Late_blight','healthy','Early_blight','Bacterial_spot')
            c.current(0)
            c.place(x=250,y=200)
        elif s=='Strawberry':
             c=ttk.Combobox(w)
             c['values']=('Leaf_scorch','healthy')
             c.current(0)
             c.place(x=250,y=200)
        elif s=='Squash':
             c=ttk.Combobox(w)
             c['values']=('Powdery_mildew')
             c.current(0)
             c.place(x=250,y=200)
        elif s=='Soybean':
             c=ttk.Combobox(w)
             c['values']=('healthy')
             c.current(0)
             c.place(x=250,y=200)
        elif s=='Raspberry':
             c=ttk.Combobox(w)
             c['values']=('healthy')
             c.current(0)
             c.place(x=250,y=200)
        elif s=='Potato':
             c=ttk.Combobox(w)
             c['values']=('Late_blight','healthy','Early_blight')
             c.current(0)
             c.place(x=250,y=200)
        elif s=='Pepper':
             c=ttk.Combobox(w)
             c['values']=('bell_healthy','bell_Bacterial_spot',)
             c.current(0)
             c.place(x=250,y=200)
        elif s=='Peach':
             c=ttk.Combobox(w)
             c['values']=('healthy','Bacterial_spot')
             c.current(0)
             c.place(x=250,y=200)
        elif s=='Orange':
             c=ttk.Combobox(w)
             c['values']=('Haunglongbing_(Citrus_greening)')
             c.current(0)
             c.place(x=250,y=200)
        elif s=='Grape':
             c=ttk.Combobox(w)
             c['values']=('Leaf_blight_(Isariopsis_Leaf_Spot)','healthy','Esca_(Black_Measles)','Black_rot')
             c.current(0)
             c.place(x=250,y=200)
        elif s=='Corn':
             c=ttk.Combobox(w)
             c['values']=('Northern_Leaf_Blight','healthy','Common_rust','Cercospora_leaf_spot')
             c.current(0)
             c.place(x=250,y=200)
        elif s=='Cherry':
             c=ttk.Combobox(w)
             c['values']=('Powdery_mildew','healthy')
             c.current(0)
             c.place(x=250,y=200)
        elif s=='Blueberry':
             c=ttk.Combobox(w)
             c['values']=('healthy')
             c.current(0)
             c.place(x=250,y=200)
        else:
            c=ttk.Combobox(w)
            c['values']=('healthy','Cedar_apple_rust','Black_rot','Apple_scab')
            c.current(0)
            c.place(x=250,y=200)
    plants.bind("<<ComboboxSelected>>", elementselected)

    w.mainloop() 

#____button find_______
btn1=Button(text="find",
background='white',
font=('arial',"14"),
fg='black',
cursor="hand1",
width=7,
command=find
)
btn1.place(x=400,y=300)
#______________window Predict _____________
def predict():
    P= Tk()
    P.geometry('500x500')
    P.resizable(False,False)
    P.title('predect')
    P.config(background='#5aa65c')
    photo=PhotoImage(file='/home/soumia/py project/img2.png')
    l = LabelFrame(P, text="predection", padx=20, pady=20,background='#5aa65c')
    l.pack(fill="both", expand="yes")
    lable1 = tkinter.Label(l, text="inserer une image",width=40)
    lable1.place(x=20,y=100)
    #____select file______
    def selectfile () :
    
      filepath = filedialog.askopenfilename(title="Ouvrir une image",filetypes=[('gif files','.gif'),('jpg files','.jpg')])
      photo =PhotoImage(filepath)
      canvas = Canvas(l, width=photo.width(), height=photo.height(), bg="yellow")
      canvas.create_image(0, 0, anchor=NW, image=photo)
      canvas.pack() 
    btn3=Button(l,text="inserer",
           background='white',
           font=('arial',"14"),
           fg='black',
           cursor="hand1",
           width=7,
          
           command=selectfile
           
    )

    btn3.place(x=370,y=100)
    #____fonction de predection____
    def rgb_bgr(image):
      rgb_img = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
      return rgb_img

    def bgr_hsv(rgb_img):
      hsv_img = cv2.cvtColor(rgb_img, cv2.COLOR_RGB2HSV)
      return hsv_img
    def img_segmentation(rgb_img,hsv_img):
      lower_green = np.array([25,0,20])
      upper_green = np.array([100,255,255])
      healthy_mask = cv2.inRange(hsv_img, lower_green, upper_green)
      result = cv2.bitwise_and(rgb_img,rgb_img, mask=healthy_mask)
      lower_brown = np.array([10,0,10])
      upper_brown = np.array([30,255,255])
      disease_mask = cv2.inRange(hsv_img, lower_brown, upper_brown)
      disease_result = cv2.bitwise_and(rgb_img, rgb_img, mask=disease_mask)
      final_mask = healthy_mask + disease_mask
      final_result = cv2.bitwise_and(rgb_img, rgb_img, mask=final_mask)
      return final_result
    def fd_hu_moments(image):
      image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
      feature = cv2.HuMoments(cv2.moments(image)).flatten()
      return feature

    def fd_haralick(image):
      gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
      haralick = mahotas.features.haralick(gray).mean(axis=0)
      return haralick    

    def fd_histogram(image, mask=None):
      image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
      hist  = cv2.calcHist([image], [0, 1, 2], None, [8, 8, 8], [0, 256, 0, 256, 0, 256])
      cv2.normalize(hist, hist)
      return hist.flatten()

    def segmnt_extrac(IMG_PATH):
      imgtest=[]
      x=[]
  
      imag= cv2.resize(IMG_PATH, tuple((256,256)))
      RGB_BGR= rgb_bgr(imag)
      BGR_HSV= bgr_hsv(RGB_BGR)
      IMG_SEGMENT   = img_segmentation(RGB_BGR,BGR_HSV)
      plt.imshow(IMG_SEGMENT)

      fv_hu_moments = fd_hu_moments(IMG_SEGMENT  )
      fv_haralick   = fd_haralick(IMG_SEGMENT )
      fv_histogram  = fd_histogram(IMG_SEGMENT  )
      imgtest= np.hstack([fv_histogram,fv_haralick, fv_hu_moments])
      x.append(imgtest)
      return   x
    from sklearn.preprocessing import MinMaxScaler

    scaler = MinMaxScaler(feature_range=(0, 1))
    #loaded_clf = joblib.load("/home/soumia/PycharmProjects/Plants_DR_RF.joblib")
    #def predect():
      #image=photo
      #image=segmnt_extrac(image)
    #img_scal=scaler.fit_transform(image)
      #y_pred=loaded_clf.predict(image)
      #messagebox.showinfo("predection","y_pred")  
      



    btn4=Button(l,text="predect",
           background='white',
           font=('arial',"14"),
           fg='black',
           cursor="hand1",
           width=7,
           #command=predect
          )
    btn4.place(x=190,y=400)
     



    P.mainloop()

#____button predict_______
btn2=Button(text="Predect",
background='white',
font=('arial',"14"),
fg='black',
cursor="hand1",
width=7,
command=predict
)
btn2.place(x=200,y=300)




window.mainloop()





